# "Hemingway" child theme for WordPress #

WordPress child theme for "Hemingway" by [Anders Norén](http://www.andersnoren.se/).

![screenshot.png](https://bytebucket.org/MarkyMarkNo1/wp-theme-hemingway-child/raw/35419880ecad352229ec91910f490b478f761b26/hemingway-custom/screenshot.png)

## Parent theme ##

* ["Hemingway" homepage](http://www.andersnoren.se/teman/hemingway-wordpress-theme/)
* ["Hemingway" demo](http://andersnoren.se/themes/hemingway/)
* ["Hemingway" on WordPress.org](http://wordpress.org/themes/hemingway)

## Child theme enhancements ##

* Sidebar gets displayed below content on smaller screens
* Jetpack plugins' [Infinite Scroll](http://jetpack.me/support/infinite-scroll/) support
* [Smart Archives Reloaded](http://wordpress.org/plugins/smart-archives-reloaded/) plugins' list format support
* [Subscribe to Comments Reloaded](https://wordpress.org/plugins/subscribe-to-comments-reloaded/) plugin support
* General layout improvements

## About WordPress ##

* [WordPress child themes](http://codex.wordpress.org/Child_Themes)
* [WordPress.org](http://wordpress.org/)
