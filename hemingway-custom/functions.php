<?php
/**
 * Hemingway custom functions
 *
 * @package hemingway
 *
 */


/*
 * =====================================================================
 *
 * Plugin Jetpack: Infinite Scroll
 *
 * http://jetpack.me/support/infinite-scroll/
 *
 * =====================================================================
 */

/*
 * If greater customization is needed, or a theme doesn’t use the content
 * template parts, a function name should be specified for the render argument
 * and that function will be used to generate the markup for Infinite Scroll.
 */
function hemingway_custom_infinite_scroll_render() {
?>
  <?php while (have_posts()) : the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <?php get_template_part( 'content', get_post_format() ); ?>

    </div> <!-- /post -->

    <?php endwhile; ?>
<?php
}

/*
 * Do we have footer widgets?
 *
 * @return bool
 */
function hemingway_custom_further_has_footer_widgets( $has_widgets ) {
  if (
    ( ( Jetpack_User_Agent_Info::is_ipad() || ( function_exists( 'jetpack_is_mobile' ) && jetpack_is_mobile() ) ) && is_active_sidebar( 'sidebar' ) ) ||
    is_active_sidebar( 'footer-a' ) ||
    is_active_sidebar( 'footer-b' ) ||
    is_active_sidebar( 'footer-c' )
  )
    return true;

  return $has_widgets;
}
add_filter( 'infinite_scroll_has_footer_widgets', 'hemingway_custom_further_has_footer_widgets' );

/*
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function hemingway_custom_setup() {

  add_theme_support( 'infinite-scroll', array(
    'container' => 'posts',
    'render' => 'hemingway_custom_infinite_scroll_render',
    'footer' => 'wrapper',
  ) );

}
add_action( 'after_setup_theme', 'hemingway_custom_setup' );
