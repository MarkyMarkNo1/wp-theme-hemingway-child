<div class="post-meta">

    <span class="post-date"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_time(get_option('date_format')); ?></a></span>

    <?php if( is_sticky() && !has_post_thumbnail() ) { ?>

        <span class="date-sep"> / </span>

        <?php _e('Sticky', 'hemingway'); ?>

    <?php } ?>

    <?php if ( current_user_can( 'manage_options' ) ) { ?>

        <span class="date-sep"> / </span>

        <?php edit_post_link(__('Edit', 'hemingway')); ?>

    <?php } ?>

</div>

<div class="post-content">

    <?php the_content(); ?>

    <?php wp_link_pages(); ?>

</div> <!-- /post-content -->

<div class="post-meta-bottom">

    <div class="post-meta">

        <!-- <span class="post-date"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_time(get_option('date_format')); ?></a></span> -->

        <!-- <span class="date-sep"> / </span> -->

        <!-- <span class="post-author"><?php the_author_posts_link(); ?></span> -->

        <!-- <span class="date-sep"> / </span> -->

        <?php comments_popup_link( '<span class="comment">' . __( '0 Comments', 'hemingway' ) . '</span>', __( '1 Comment', 'hemingway' ), __( '% Comments', 'hemingway' ) ); ?>

        <!--
        <?php if( is_sticky() && !has_post_thumbnail() ) { ?>

            <span class="date-sep"> / </span>

            <?php _e('Sticky', 'hemingway'); ?>

        <?php } ?>
        -->

        <?php if ( current_user_can( 'manage_options' ) ) { ?>

            <span class="date-sep"> / </span>

            <?php edit_post_link(__('Edit', 'hemingway')); ?>

        <?php } ?>

    </div>

</div> <!-- /post-meta-bottom -->

<div class="clear"></div>
